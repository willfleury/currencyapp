/*
 */
package unit;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import models.Currency;
import models.Rate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.junit.Test;
import play.test.UnitTest;

/**
 *
 * @author Will
 */
public class RateTest extends UnitTest {

    private static final DateTimeFormatter dateFormatter = DateTimeFormat.forPattern("yyyy-MM-dd");
    
    @Test
    public void testCalculateRate() {
        BigDecimal gbp = new BigDecimal("0.86055");
        BigDecimal usd = new BigDecimal("1.2883");
        
        BigDecimal result = Rate.calculateRate(
                new Rate("EUR", null, gbp), new Rate("EUR", null, usd), 
                Currency.DEFAULT_DECIMAL_SCALE);
        
        //no need to do .stripTrailingZeros() here as its full to the 6th decimal place!
        assertEquals(new BigDecimal("1.497066"), result);
    }
    
    
    @Test
    public void testSingleDAO() {
        Date now = new Date();
        Rate rate1 = new Rate("MADEUP", now, new BigDecimal("1.245"));
        
        rate1.save();
        
        Rate rate2 = Rate.find("MADEUP", now);
        
        //cleanup before asserts
        rate1.delete();
        
        assertNotNull(rate2);
        assertEquals(rate1, rate2);
    }
    
    @Test
    public void testBulkDAO() {
        Rate rate1 = new Rate("MADEUP", dateFormatter.parseDateTime("2013-07-10").toDate(), new BigDecimal("1.2"));
        Rate rate2 = new Rate("MADEUP", dateFormatter.parseDateTime("2013-08-10").toDate(), new BigDecimal("1.2"));
        
        Rate.upsertAll(Arrays.asList(rate1, rate2));
        
        List<Rate> rates = Rate.findAll("MADEUP");
        
        //cleanup before asserts
        rate1.delete();
        rate2.delete();
        
        assertEquals(2, rates.size());
        
        assertTrue(rates.contains(rate1));
        assertTrue(rates.contains(rate2));
    }
    
}
