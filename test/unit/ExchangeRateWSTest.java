/*
 */
package unit;

import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import models.Rate;
import org.junit.Test;
import play.test.UnitTest;
import util.ExchangeRateWS;
import util.async.SmartCallback;

/**
 *
 * @author Will
 */
public class ExchangeRateWSTest extends UnitTest {
    
    @Test
    public void testDaily() throws Exception {
        final CountDownLatch latch = new CountDownLatch(1);
        
        //I'll just demo the callback async feature here..
        
        ExchangeRateWS.getHistoricAsync(new SmartCallback<Map<String, List<Rate>>>() {
            
            public void onCompleted(Map<String, List<Rate>> result) {
                assertNotNull(result);
                assertTrue(!result.get("GBP").isEmpty());
                
                latch.countDown();
            }

            public void onThrowable(Throwable t) {
                throw new RuntimeException(t);
            }
        });
        
        latch.await();
    }
    

}
