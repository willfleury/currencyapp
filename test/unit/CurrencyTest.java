package unit;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import org.junit.*;
import models.Currency;
import play.test.*;

public class CurrencyTest extends UnitTest {

    @Test
    public void testConvert() {
        BigDecimal amt = new BigDecimal("10.0");
        BigDecimal rate = new BigDecimal("1.5");
        
        BigDecimal result1 = Currency.convert(amt, rate);
        assertEquals(new BigDecimal("15"), result1.stripTrailingZeros());
        
        //ensure both methods return same values with same scaling
        BigDecimal result2 = Currency.convert(amt, rate, Currency.DEFAULT_DECIMAL_SCALE);
        assertEquals(result1.stripTrailingZeros(), result2.stripTrailingZeros());
    }
    
    
    
    @Test
    public void testSingleDAO() {
        Date now = new Date();
        
        Currency currency1 = new Currency("MADEUP");
        currency1.lastRefresh = now;
        currency1.lastRateRefresh = now;
        currency1.save();
        
        Currency currency2 = Currency.find("MADEUP");
        
        //cleanup before asserts
        currency1.delete();
        
        assertNotNull(currency2);
        assertEquals(currency1, currency2);
        assertEquals(currency1.lastRateRefresh, currency2.lastRateRefresh);
    }
    
    @Test 
    public void testBulkDAO() {
        Date now = new Date();
        
        Currency currency1 = new Currency("MADEUP1");
        currency1.lastRefresh = now;
        currency1.lastRateRefresh = now;
      
        
        Currency currency2 = new Currency("MADEUP2");
        currency2.lastRefresh = now;
        currency2.lastRateRefresh = now;

        Currency.upsertBatch(Arrays.asList(currency1, currency2));
        
        Collection<Currency> currencies = Currency.findAll();
        
        //cleanup before asserts
        currency1.delete();
        currency2.delete();
        
        assertTrue(currencies.size() >= 2);
        assertTrue(currencies.contains(currency1));
        assertTrue(currencies.contains(currency2));
    }

}
