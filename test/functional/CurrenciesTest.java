package functional;


import org.junit.Test;
import play.mvc.Http;
import play.test.FunctionalTest;

/*
 */

/**
 * Only a few sample test 
 * @author Will
 */
public class CurrenciesTest extends FunctionalTest {
    
    

    @Test
    public void testThatIndexPageWorks() {
        Http.Response response = GET("/");
        assertIsOk(response);
        assertContentType("text/html", response);
        assertCharset(play.Play.defaultWebEncoding, response);
    }
    
    
    @Test
    public void testList() {
        Http.Response response = GET("/currencies/list");
        assertIsOk(response);
        assertContentType("application/json; charset=utf-8", response);
    }
    
}