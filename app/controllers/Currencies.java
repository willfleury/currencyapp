/*
 */
package controllers;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.locks.ReentrantLock;
import models.Currency;
import models.Rate;
import org.apache.log4j.Logger;
import play.Play;
import play.data.binding.As;
import play.mvc.Before;
import play.mvc.Controller;
import util.DateUtil;
import util.ExchangeRateWS;
import util.async.ThreadPoolUtil;

/**
 * This is the sole controller for the application. It looks after all tasks.
 * 
 * The majority of actions render JSON as the methods are called via AJAX and return JSON. 
 * 
 * @author Will
 */
public class Currencies extends Controller {
    
    private static final Logger logger = Logger.getLogger(Currencies.class);
    
    /**
     * number of maximum concurrent processing threads for service requests
     */
    private static final String CONCURRENCY_LEVEL = "service.controller.concurrency.level";
    
    /**
     * maximum number of tasks which can be in the backlog for processing before we start rejecting
     */
    private static final String BACKLOG_THRESHOLD = "service.controller.concurrency.backlog";
    
    /**
     * Defaults for both the concurrency properties
     */
    private static final String DEFAULT_SERVICE_CONCURRENCY = "10";
    
    private static final String DEFAULT_MAX_ACCEPT_BACKLOG = "1000";
    
    
    
    /**
     * Thread pool executor service which is used to handle the processing of requests
     * tasks handed off from the play request handler. It does the heavy lifting.
     * Max backlog is set so that we can't have tasks continuing to increase in backlog.
     */
    private static final ThreadPoolExecutor executor =  ThreadPoolUtil.createFixedSizeAndTaskBacklogThreadPool(
            Integer.parseInt(Play.configuration.getProperty(CONCURRENCY_LEVEL, DEFAULT_SERVICE_CONCURRENCY)), 
            Integer.parseInt(Play.configuration.getProperty(BACKLOG_THRESHOLD, DEFAULT_MAX_ACCEPT_BACKLOG)));
   
    
    /**
     * Duration in days for the "historic" period in the UI
     */
    private static final int  HISTORIC_PERIOD_DAYS = 90;
    
    /**
     * Used for managing exclusive initialization to one thread
     */
    private static final ReentrantLock lock = new ReentrantLock();
    
    /**
     * Indicates if initialization is complete or not - volatile important!
     */
    private static volatile boolean initializationComplete = false;
    

    /**
     * creates a Gson obj with the correct date formatter we want - couldn't see
     * wheret to configure in renderer in play - also coulndt find a global setJson etc.
     * Gson instances are thread safe so lets just create once.
     */
    private static final Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
   
    
    /**
     * We want to ensure no matter what api method is called that the initialization is 
     * performed on the first request - this is a nice place to put interceptor
     */
    @Before
    static void checkInitialized() {
        ensureInitialLoadPerformed();
    }
    
    
    /**
     * returns available currencies for the page to render
     */
    public static void index() {            
        Collection<String> currencies = getAvailableCurrencies();
        render(currencies);
    }
    
    
    /**
     * List all available currencies to use and renders as json
     */
    public static void list() {
        renderJSON(getAvailableCurrencies());
    }
    
    
    /**
     * Returns the available dates which we can convert currencies for
     * 
     * Dates are returned in yyyy-MM-dd format
     */
    public static void availableDates() {
        //Lets get the dates there for all the EUR entries (i.e always there)
        Future<List<Date>> future = executor.submit(new Callable() {
            public List<Date> call() throws Exception {
                return Rate.findAvailableDates();
            }
        });  
        
        //return dates in the correct format we want.
        renderJSON(gson.toJson(await(future)));
    }
    
    
    /**
     * Returns the fx rates of the selected currency against the EUR for the 
     * last n days.
     * 
     * @param ccy  currency to get rate for
     */
    public static void fetchHistoric(final String ccy) {
        Future<List<Rate>> future = executor.submit(new Callable() {

            public List<Rate> call() throws Exception {
                //ensure they are looking for a valid currency
                Currency currency = checkCurrencyExists(ccy);  
                
                List<Rate> rates = null;
           
                //possibly out of date - try refresh if not refreshed today
                Date lastRefresh = currency.lastRateRefresh;
                if (lastRefresh == null || !DateUtil.isToday(lastRefresh)) {
                    //call the task async so throttling is invoked but block on future until it returns
                    Map<String, List<Rate>> historic = ExchangeRateWS.getHistoricAsync().get();
                    errorIfNull(historic, "FX service not available");

                    rates = historic.get(ccy);
                    
                    //roll up rates for this currency
                    Rate.rollup(rates);      
                    
                    //update last_rate_refresh 
                    currency.lastRateRefresh = DateUtil.getDateComponent(new Date());
                    currency.save();
                } else {
                    //ok now lets get whatever is in the db for this exchange
                    rates = Rate.findAll(ccy, HISTORIC_PERIOD_DAYS);
                }
                
                return rates;
            }
        });

        List<Rate> rates = await(future);
        
        //see if we found them
        notFoundIfNull(rates, "Could not find rates for " + ccy); 
        
        renderJSON(gson.toJson(rates));
    }
    
    
    
    
    
    /**
     * Converts the amount from the source currency into the target currency for the 
     * given date.
     * 
     * @param sourceCcy source currency
     * @param targetCcy target currency
     * @param amt amount to convert
     * @param date  date of fx to use
     */
    public static void convert(final String sourceCcy, final String targetCcy, 
            final String amt, @As("yyyy-MM-dd") final Date date) {
        Future<BigDecimal> future = executor.submit(new Callable() {

            public BigDecimal call() throws Exception {
                checkCurrencyExists(sourceCcy);
                checkCurrencyExists(targetCcy);

                //find rates
                Rate sourceRate = Rate.find(sourceCcy, date);
                Rate targetRate = Rate.find(targetCcy, date);

                //ennsure rates for those dates exist
                notFoundIfNull(sourceRate, "No rate available for that date");
                notFoundIfNull(targetRate, "No rate available for that date");

                //lets calculate the rate based on source to target currencies
                BigDecimal rate = Rate.calculateRate(
                        sourceRate, targetRate, Currency.DEFAULT_DECIMAL_SCALE);

                //make BigDecimal out of amount via string constructor and do actual conversion
                return Currency.convert(new BigDecimal(amt), rate);
            }
        });
        
        BigDecimal result = await(future);
        
        //now return result
        renderJSON(result);
    }
    
    
    /**
     * Forces a refresh of the daily data. Note that based on the requirements
     * this should only refresh a single currencies data but given we must fetch
     * all currencies data from the ECB to get just one we might as well refresh
     * the whole lot. So this method and {@link #refreshRates() } are essentially
     * identical. 
     * 
     * The only difference is it returns the specified currencies value. 
     * 
     * @param ccy the currency to refresh rates for
     */
    public static void refreshRate(final String ccy) {
        Future<Rate> future = enqueue(new Callable() {
            
            public Rate call() throws Exception {
                checkCurrencyExists(ccy);
            
                Map<String, List<Rate>> rates = ExchangeRateWS.getDailyAsync().get();
                errorIfNull(rates, "FX service not available");

                Rate rate = rates.get(ccy).iterator().next();
                //now store again incase
                rate.save();
                
                //don't update last_rate_refresh after daily refresh - only after historic
                return rate;
            }
        });
        
        //let processing thread go on
        Rate rate = await(future);        
        
        //when we update daily rates only one entry in the map of rates
        renderJSON(gson.toJson(rate));
    }
    
    
    /**
     * Same as {@link #refreshRate(java.lang.String)} except it returns all rates 
     * once refreshed
     */
    public static void refreshRates() {        
        Future<Map<String, List<Rate>>> future = enqueue(new Callable() {
            
            public Map<String, List<Rate>> call() throws Exception {
                Map<String, List<Rate>> rates = ExchangeRateWS.getDailyAsync().get();
                errorIfNull(rates, "FX service not available");
                
                //now store refreshed list
                for (String c : rates.keySet()) {
                    Rate.upsertAll(rates.get(c));
                    //don't update last_rate_refresh after daily refresh - only after historic
                }
                
                return rates;
            }
        });
         
        //let request thread go on
        Map<String, List<Rate>> rates = await(future);
        
        //when we update daily rates only one entry in the map of rates
        renderJSON(gson.toJson(rates));
    }
    
    
    /**
     * Gets a list of the currently available currencies. It first checks the 
     * database for them and if they do not exist or the last_refresh of an 
     * arbitrary stock is a previous day it performs a refresh from the service.
     * 
     * By checking last_refresh it means we will always keep things up to date 
     * without causing unnecessary requests going to the third party api.
     * 
     * @return available currencies
     */
    private static Collection<String> getAvailableCurrencies() {
        Future<Collection<String>> future = enqueue(new Callable() {
            
            public Collection<String> call() throws Exception {
                //look in database first 
                Collection<Currency> currencies = Currency.findAll();
                
                // if not stored yet then look for it from third party (will always be in db though!)
                if (currencies.isEmpty() || 
                        DateUtil.isToday(currencies.iterator().next().lastRefresh)) {
                    //call api and block while waiting for result 
                    currencies = Currency.fromSymbols(
                            ExchangeRateWS.getAvailableCurrenciesAsync().get(), new Date());
                    errorIfNull(currencies, "FX service not available");
                    
                    //store them - we could get batch methods to return future also but this isn't
                    //request handler thread - maybe we should though..
                    Currency.upsertBatch(currencies);
                }
                
                return Currency.toSymbols(currencies);
            }
        });
        
        Collection<String> currencySymbols = await(future);
        
        return currencySymbols;
    }
    
   
    /**
     * Utility method to check if the supplied currency exists (in our set).
     * It throws a 404 if it isn't.
     * 
     * @param ccy the currency to check exists
     */
    private static Currency checkCurrencyExists(String ccy) {        
        Currency currency = Currency.find(ccy);
        
        if (currency == null) //throw correct http error if not found
            notFound("Currency not available!");     
        
        return currency;
    }
    
    
    /**
     * Enqueues a task for execution by the service thread pool if there is 
     * available capactiy left in the thread pool queue.
     * 
     * @param <T>
     * @param callable the task to run
     * @return a future for the execution of the task
     * 
     * @throws RuntimeException if there is no capacity left to make this non-blocking call
     */
    private static <T> Future<T> enqueue(Callable<T> callable) {
        return ThreadPoolUtil.checkServiceBacklogAndEnqueue(executor, callable);
    }
    
    
    
    /**
     * Performs the initialization of the service. To ensure we don't have multiple
     * clients causing an initialization at the same time we make use of locks.
     * 
     * A boolean initialization complete flag is checked on all requests to ensure
     * its not performed more than once.
     */
    private static void ensureInitialLoadPerformed() {
        if (initializationComplete) return;
        
        try {
            lock.lock();            
            
            if (initializationComplete) return;
            //do a bulk load of last 90 days and currencies
            
            Collection<Currency> currencies = Currency.fromSymbols(
                    ExchangeRateWS.getAvailableCurrenciesAsync().get(), new Date());
            errorIfNull(currencies, "FX service not available");
            
            Currency.upsertBatch(currencies);

            //load one days at least 
            Map<String, List<Rate>> rates = ExchangeRateWS.getHistoricAsync().get();
            errorIfNull(rates, "FX service not available");
            
            for (String ccy : rates.keySet()) 
                Rate.upsertAll(rates.get(ccy));
           
            initializationComplete = true;
        } catch (Exception e) { 
            throw new RuntimeException("Error during initilization", e);
        } finally {
            lock.unlock();
        }
        
    }
    
    /**
     * Sends a 500 response if the obj is null
     * 
     * @param obj
     * @param desc 
     */
    private static void errorIfNull(Object obj, String desc) {
        if (obj == null) {
            error(desc);
        }
    }
}
