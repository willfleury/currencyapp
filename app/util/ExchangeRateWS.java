/*
 */
package util;

import play.Logger;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;
import models.Rate;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.helpers.XMLReaderFactory;
import play.Play;
import play.libs.WS;
import play.libs.WS.HttpResponse;
import util.async.SmartCallback;
import util.async.SmartCallbackTask;
import util.async.ThreadPoolUtil;

/**
 *
 * Provides an utility for accessing the European Central Bank's
 * (ECB) foreign exchange rates. The published ECB rates contain exchange rates
 * for approx. 35 of the world's major currencies. They are updated daily at
 * 15:00 CET. These rates use EUR as reference currency and are specified with a
 * precision of 1/10000 of the currency unit (one hundredth cent). See:
 *
 * http://www.ecb.int/stats/exchange/eurofxref/html/index.en.html
 * 
 * The API provides async methods which will prevent the API from being run 
 * over during peak times. If requests go over the allowed thresholds it simply
 * rejects new requests until it has capacity again. 
 * 
 * Async API methods provide Future's or Callback handlers as a means of obtaining
 * the results.
 * 
 * This prevents bottlenecks  here from causing pressure on other parts of the system.
 * 
 * The configuration properties available are 
 *      service.exchange.concurrency.level=5
 *      service.exchange.concurrency.backlog=100
 * 
 * 
 * @author Will
 */
public class ExchangeRateWS {
    
    
    /**
     * number of maximum concurrent processing threads for service requests
     */
    private static final String CONCURRENCY_LEVEL = "service.exchange.concurrency.level";
    
    /**
     * maximum number of tasks which can be in the backlog for processing before we start rejecting
     */
    private static final String BACKLOG_THRESHOLD = "service.exchange.concurrency.backlog";
    
    /**
     * Defaults for both the concurrency properties
     */
    private static final String DEFAULT_SERVICE_CONCURRENCY = "5";
    
    private static final String DEFAULT_MAX_ACCEPT_BACKLOG = "100";
    
    
    /**
     * Thread pool executor service which is used to handle the processing of web 
     * service calls. 
     * Max backlog is set so that we can't have tasks continuing to increase in backlog 
     * and eventually it will stop accepting new requests
     */
    public static final ThreadPoolExecutor executor =  ThreadPoolUtil.createVariableSizeAndTaskBacklogThreadPool(
            1, //always leave one started ready to go
            Integer.parseInt(Play.configuration.getProperty(CONCURRENCY_LEVEL, DEFAULT_SERVICE_CONCURRENCY)), 
            Integer.parseInt(Play.configuration.getProperty(BACKLOG_THRESHOLD, DEFAULT_MAX_ACCEPT_BACKLOG)));
    
    
    
    /**
     * 90 day trading rates url
     */
    public static final String ECB_HISTORIC_90_DAYS = "http://www.ecb.europa.eu/stats/eurofxref/eurofxref-hist-90d.xml";
    
    /**
     * Last trading days rates url
     */
    public static final String ECB_DAILY = "http://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml";
    
    

     /**
     * Calls the service async and returns a future for the latest exchange data.
     * @return a Future for the data
     */
    public static Future<Map<String, List<Rate>>> getDailyAsync() {
        return enqueue(new Callable<Map<String, List<Rate>>>() {
            public Map<String, List<Rate>> call() throws Exception {
                return getDaily();
            }
        });
    }
    
    
    /**
     * Call service async and the callback supplied will be called when complete.
     * @param callback 
     */
    public static void getDailyAsync(SmartCallback<Map<String, List<Rate>>> callback) {
        enqueueCallback(callback, new Callable<Map<String, List<Rate>>>() {
            public Map<String, List<Rate>> call() throws Exception {
                return getDaily();
            }
        });
    }

    
    /**
     * Calls the service async and returns a future for the latest exchange data.
     * @return a Future for the data
     */
    public static Future<Map<String, List<Rate>>> getHistoricAsync() {        
        return enqueue(new Callable<Map<String, List<Rate>>>() {
            public Map<String, List<Rate>> call() throws Exception {
                return getHistoric();
            }
        });
    }
    
    /**
     * Call service async and the callback supplied will be called when complete.
     * @param callback 
     */
    public static void getHistoricAsync(SmartCallback<Map<String, List<Rate>>> callback) {
        enqueueCallback(callback, new Callable<Map<String, List<Rate>>>() {
            public Map<String, List<Rate>> call() throws Exception {
                return getHistoric();
            }
        });
    }
    
    /**
     * Calls the service async and returns a future for the latest exchange data.
     * @return a Future for the data
     */
    public static Future<Collection<String>> getAvailableCurrenciesAsync() {
        return enqueue(new Callable<Collection<String>>() {
            public Collection<String> call() throws Exception {
                return getAvailableCurrencies();
            }
        });
    }
    
    /**
     * Call service async and the callback supplied will be called when complete.
     * @param callback 
     */
    public static void getAvailableCurrenciesAsync(SmartCallback<Collection<String>> callback) {
        enqueueCallback(callback, new Callable<Collection<String>>() {
            public Collection<String> call() throws Exception {
                return getAvailableCurrencies();
            }
        });
    }
    
    
    
    protected static Map<String, List<Rate>> getDaily() {
        return new ExchangeRateWS().fetchRates(ECB_DAILY);
    }
    
    protected static Map<String, List<Rate>> getHistoric() {      
        return new ExchangeRateWS().fetchRates(ECB_HISTORIC_90_DAYS);
    }
    
    
    protected static Collection<String> getAvailableCurrencies() {
        Map<String, List<Rate>> rates = getDaily();
        return rates == null ? null : rates.keySet();
    }
    
    
    protected static <T> Future<T> enqueue(Callable<T> callable) {
        return ThreadPoolUtil.checkServiceBacklogAndEnqueue(executor, callable);
    }
    
    protected static void enqueue(Runnable runnable) {
        ThreadPoolUtil.checkServiceBacklogAndEnqueue(executor, runnable);
    }
    
    protected static <T> void enqueueCallback(SmartCallback callback, Callable<T> callable) {        
        ThreadPoolUtil.checkServiceBacklogAndEnqueue(executor, new SmartCallbackTask<T>(callback, callable));
    }
    
    
    /** 
     * End of static helper methods. 
     */
    
    
    
    public ExchangeRateWS() { }
    
    /**
     * Is used to fetch the exchange rates from the ECB using the url provided
     * (i.e. daily or historic). 
     * 
     * @param url the url to fetch data from
     * @return the parsed data or null if not found/service was unavailable
     */
    private Map<String, List<Rate>> fetchRates(String url) {
        HttpResponse response = WS.url(url).get();
        
        //return null if it wasn't a successful http response
        Map<String, List<Rate>> rates = 
                response != null && response.success() ?       
                parse(response.getStream()) 
                : null;

        return rates;
    }
    
    
    /**
     * Parse the input stream which contains the ECB rates.
     * Use SAX as its quite big to parse all into memory at same time using
     * DOM (for historic)
     * 
     * @param stream the stream where the data is available
     * 
     * @return the resulting currencies 
     */
    private Map<String, List<Rate>> parse(InputStream stream) {
        Map<String, List<Rate>> currencies = null;
        
        try {
            //setup
            XMLReader saxReader = XMLReaderFactory.createXMLReader();
            ECBParserHandler handler = new ECBParserHandler();              
           
            //now parse it from the stream 
            saxReader.setContentHandler(handler);
            saxReader.setErrorHandler(handler);
            saxReader.parse(new InputSource(stream));
            
            //set the rates
            currencies = handler.currencies;

        } catch(Exception e) {
            Logger.error(e, "Error parsing ECB fx data..");
            
            //return null
            return null;
        } finally {
            //make sure to close the stream
            try { if (stream != null) stream.close(); } catch (IOException e) { }
        }
        
        return currencies;
    }
    
    
    /**
     * This is a SAX handler implementation for parsing the data from the ECB
     */
    private class ECBParserHandler extends DefaultHandler {
        
        /**
         * Used to hold date being parsed
         */
        Date refDate;
        
        /**
         * map of all rates. key = List<Rate> (handy for quick lookup - could have used hashset)
         */
        final Map<String, List<Rate>> currencies = new HashMap<String, List<Rate>>();        
        
        
        public ECBParserHandler() {}
        
        @Override
        public void startElement(String uri, String localName,
                String qName, Attributes attributes) {
            
            //Cube elements can contain just time attributes OR currency/rates
            if (localName.equals("Cube")) {
                String date = attributes.getValue("time");
                //if time present then extract it so we can attatch to rates 
                //we pull for that date
                if (date != null) {
                    refDate = DateUtil.parseDate(date);           //get date                      
                   
                    //add the euro entry
                    addRate("EUR", refDate, "1.0");
                }

                String ccy = attributes.getValue("currency");
                String fx = attributes.getValue("rate");
                if (ccy != null && fx != null) {
                    addRate(ccy, refDate, fx);
                }
            }
        }
        
        
        /**
         * Adds a rate to the list of rates for the currency.
         * 
         * @param ccy the currency
         * @param date the date of the rate
         * @param fx the rate 
         */
        private void addRate(String ccy, Date date, String fx) {
            //check if already exists , if not create
            List<Rate> rates = currencies.get(ccy);
            if (rates == null) {
                rates = new ArrayList<Rate>();
                currencies.put(ccy, rates);
            }                         

            Rate rate = new Rate(ccy, date, 
                    new BigDecimal(fx));//don't forget to use string constructor

            rates.add(rate);
        }
    }

}
