/*
 */
package util.async;

import java.util.concurrent.Callable;

/**
 * Callback Task which takes a task and a Callback as arguments. When executed
 * it executes the provided task and calls the callback upon completion. 
 * 
 * @author Will
 */
public class SmartCallbackTask<T> implements Runnable {
    
    private final Callable<T> task;
    private final SmartCallback<T> callback;
    
    public SmartCallbackTask(SmartCallback<T> callback, Callable<T> task) {
        this.callback = callback;
        this.task = task;
    }

    public void run() {
        try {
            T result = task.call();      
            callback.onCompleted(result);
        } catch (Exception e) {
            callback.onThrowable(e);
        }
    }
}
