/*
 */
package util.async;

/**
 * Callback interface for async tasks.
 * 
 * @author Will
 */
public interface SmartCallback<T> {
    
    /**
     * Called on successful completion of the task 
     * @param result 
     */
    public void onCompleted(T result);
    
    /**
     * Called when there is a failure in the processing of the task
     * @param t 
     */
    public void onThrowable(Throwable t);
}
