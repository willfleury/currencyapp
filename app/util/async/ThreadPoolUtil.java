/*
 */
package util.async;

import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * Executor service helper methods.
 * 
 * @author Will
 */
public class ThreadPoolUtil {
    
    /**
     * Create a thread pool with a fixed number of threads and the specified max 
     * task backlog (queue depth).
     * 
     * @param numThreads number of thread for thread pool
     * @param maxDepth max depth of queue (backlog)
     * @return ThreadPoolExecutor
     */
    public static ThreadPoolExecutor createFixedSizeAndTaskBacklogThreadPool(int numThreads, int maxDepth) {
        return new ThreadPoolExecutor(numThreads,  numThreads,
            0L, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<Runnable>(maxDepth));
    }
    
    
    /**
     * Create a thread pool with a variable number of threads (min / max) and the 
     * specified max task backlog (queue depth).
     * 
     * @param numCoreThreads core number of threads (min)
     * @param numMaxThreads max number of thread for thread pool
     * @param maxDepth max depth of queue (backlog)
     * @return ThreadPoolExecutor
     */
    public static ThreadPoolExecutor createVariableSizeAndTaskBacklogThreadPool(int numCoreThreads, int numMaxThreads,  int maxDepth) {
        return new ThreadPoolExecutor(numCoreThreads,  numMaxThreads,
            0L, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<Runnable>(maxDepth));
    }
    
  
    
    /**
     * If the provided ThreadPoolExecutor has some capacity left (i.e. the task queue 
     * is not full and so submit isn't a blocking operation), it will add the task 
     * to the queue and return the future.
     * 
     * If it has no remaining capacity it does not block throws a RuntimeException 
     * 
     * @param <T>
     * @param executor the ThreadPoolExecutor to submit to
     * @param call the task
     * @return a Future for the tasks completion
     * 
     * @throws RuntimeException if the thread pool has no remaining capacity to enqueue without blocking
     */
    public static <T> Future<T> checkServiceBacklogAndEnqueue(ThreadPoolExecutor executor, Callable call) {
        if (executor.getQueue().remainingCapacity() <= 0)
            throw new RuntimeException("High volumes currently being experienced..");
        
        return executor.submit(call);
    }
    
    
    /**
     * If the provided ThreadPoolExecutor has some capacity left (i.e. the task queue 
     * is not full and so submit isn't a blocking operation), it will add the task 
     * to the queue and return the future.
     * 
     * If it has no remaining capacity it does not block throws a RuntimeException 
     * 
     * @param <T>
     * @param executor the ThreadPoolExecutor to submit to
     * @param call the task
     * @return a Future for the tasks completion
     * 
     * @throws RuntimeException if the thread pool has no remaining capacity to enqueue without blocking
     */
    public static void checkServiceBacklogAndEnqueue(ThreadPoolExecutor executor, Runnable runnable) {
        if (executor.getQueue().remainingCapacity() <= 0)
            throw new RuntimeException("High volumes currently being experienced..");
        
        executor.submit(runnable);
    }
}
