/*
 */
package util;

import java.util.Date;
import org.apache.commons.lang.time.DateUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

/**
 * Date helper methods for dealing with dates in the format yyyy-MM-dd
 * 
 * @author Will
 */
public class DateUtil {
    
    /**
     * Joda time DateTimeFormatter used as it is both efficient and thread-safe unlike
     * java.util.DateFormat
     */
    public static final DateTimeFormatter dateFormatter = DateTimeFormat.forPattern("yyyy-MM-dd");
    
    /**
     * Extracts the Date component from the supplied Date object and returns the 
     * new Date object which represents date only.
     * 
     * @param date 
     * @return 
     */
    public static Date getDateComponent(Date date) {
        return dateFormatter.parseDateTime(dateFormatter.print(new DateTime())).toDate();
    }
    
    /**
     * Parses strings in the format yyyy-MM-dd and returns the date object
     * 
     * @param str string representing date
     * @return the date
     */
    public static Date parseDate(String str) {
        return dateFormatter.parseDateTime(str).toDate();
    }
    
    /**
     * Is the supplied date today
     * 
     * @param date
     * @return 
     */
    public static boolean isToday(Date date) {
        return DateUtils.isSameDay(new Date(), date);
    }
}
