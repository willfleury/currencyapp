/*
 */
package models;

import com.netflix.astyanax.annotations.Component;
import com.netflix.astyanax.connectionpool.OperationResult;
import com.netflix.astyanax.connectionpool.exceptions.ConnectionException;
import com.netflix.astyanax.model.ColumnFamily;
import com.netflix.astyanax.model.ColumnList;
import com.netflix.astyanax.model.CqlResult;
import com.netflix.astyanax.model.Row;
import com.netflix.astyanax.serializers.StringSerializer;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import models.db.AstyanaxInstance;
import models.db.AstyanaxFactory;
import org.apache.log4j.Logger;

/**
 * Model for a exchange rate.
 * 
 * This class acts as a DAO for Cassandra DB also and provides lots of helper 
 * methods for working with rates. 
 * 
 * @author Will
 */
public class Rate  {
    
    private static final Logger logger = Logger.getLogger(Rate.class);
    
    
    /**
     * astynax seems to have buffer overflow issues with using CompositeKeys
     * in the version being used and many other issues around composite keys
     * /https://github.com/Netflix/astyanax/issues/80
     */
    public static class CompositeKey {
        @Component (ordinal=0)
        public String currency;
        
        @Component (ordinal=1)
        public long date;
        
        public CompositeKey() {  }
        public CompositeKey(String currency, long date) {
            this.currency = currency;
            this.date = date;
        }
    }
    
  
    
    private static final String CF_NAME = "rates";
    
    private static final String UPSERT_STATEMENT = String.format("INSERT INTO %s (currency, date, rate) VALUES (?, ?, ?)", CF_NAME);     
    
    private static final String FIND_CCY_RATES_STATEMENT = String.format("SELECT date, rate FROM %S WHERE currency = ? ORDER BY date DESC", CF_NAME);
    
    private static final String FIND_RATE_STATEMENT = String.format("SELECT rate FROM %S WHERE currency = ? AND date = ?", CF_NAME);
    
    private static final String FIND_LATEST_DATE_STATEMENT = String.format("SELECT date, rate FROM %S WHERE currency = ? ORDER BY date DESC LIMIT 1", CF_NAME); 
    
    private static final String FIND_ALL_DATES_STATEMENT = String.format("SELECT date FROM %S WHERE currency = 'EUR' ORDER BY date DESC", CF_NAME); 
    
    private static final String DELETE_STATEMENT = String.format("DELETE FROM %S WHERE currency = ? AND date = ?", CF_NAME);
    
    
//    private static final ColumnFamily<CompositeKey, String> CF = ColumnFamily.newColumnFamily(
//                CF_NAME, 
//                new AnnotatedCompositeSerializer<CompositeKey>(CompositeKey.class,),
//                StringSerializer.get());
    
    private static final ColumnFamily<String, String> CF = ColumnFamily.newColumnFamily(
                CF_NAME, 
                StringSerializer.get(),
                StringSerializer.get());
    
    
    /**
     * Easy to convert source to target rates as we know we have all rates 
     * normalized to the EUR. Uses RoundingMode.HALF_UP
     * 
     * @param source source rate vs EUR
     * @param target target rate vs EUR
     * @param precision 
     * 
     * @return the rate to convert from source to target
     */
    public static BigDecimal calculateRate(Rate source, Rate target, int precision) {
        return target.rate.divide(source.rate, precision, RoundingMode.HALF_UP);
    }
    
    
    public String currency;
    
    /**
     * the date of this rate (only storing eod so could just format to date only!)
     */
    public Date date;
    
     /** 
      * lets be safe about storing the rate so lets use BigDecimal
     */
    public BigDecimal rate; 
    
    
    public Rate() { }
    
    public Rate(String ccy, Date date, BigDecimal rate) {
        this.currency = ccy;
        this.date = date;
        this.rate = rate;
    }

    
    /**
     * DAO Methods
     */
    

    /**
     * either insert or update (upsert)
     */
    public void save() {
        AstyanaxInstance astyanax = AstyanaxFactory.getDefault();
        
        try {
            @SuppressWarnings("unused")
            OperationResult<CqlResult<String, String>> result = astyanax.getDefaultKeyspace()
                    .prepareQuery(CF)
                    .withCql(UPSERT_STATEMENT)
                    .asPreparedStatement()
                    .withStringValue(currency)
                    .withLongValue(date.getTime()) //pass it as time since epoc (unix time)
                    .withStringValue(rate.toString())
                    .execute();
        } catch (ConnectionException e) {
            throw new RuntimeException("failed to persist rate", e);
        }
    }
    
    public void delete() {
        AstyanaxInstance astyanax = AstyanaxFactory.getDefault();
        
        try {
            @SuppressWarnings("unused")
            OperationResult<CqlResult<String, String>> result = astyanax.getDefaultKeyspace()
                    .prepareQuery(CF)
                    .withCql(DELETE_STATEMENT)
                    .asPreparedStatement()
                    .withStringValue(currency)
                    .withLongValue(date.getTime()) 
                    .execute();
        } catch (ConnectionException e) {
            throw new RuntimeException("failed to delete rate", e);
        }
    }
    

    
    public static Rate find(String ccy, Date date) {
        AstyanaxInstance astyanax = AstyanaxFactory.getDefault();

        try {
            OperationResult<CqlResult<String, String>> result = astyanax.getDefaultKeyspace()
                    .prepareQuery(CF)
                    .withCql(FIND_RATE_STATEMENT)
                    .asPreparedStatement()
                    .withStringValue(ccy)
                    .withLongValue(date.getTime())
                    .execute();
            for (Row<String, String> row : result.getResult().getRows()) {
                ColumnList<String> cols = row.getColumns();
                
                Rate rate = new Rate();
                rate.currency = ccy;
                rate.date = new Date(date.getTime()); 
                rate.rate = new BigDecimal(cols.getStringValue("rate", null)); //NPE if not there - don't hide error like that, let it fail
                
                return rate;
            }
        } catch (ConnectionException e) {
            throw new RuntimeException("failed to find all rates for " + ccy, e);
        }
        
        return null;
    }
    
    public static Rate findLatestRate(String ccy) {
        AstyanaxInstance astyanax = AstyanaxFactory.getDefault();
         
        try {
            OperationResult<CqlResult<String, String>> result = astyanax.getDefaultKeyspace()
                    .prepareQuery(CF)
                    .withCql(FIND_LATEST_DATE_STATEMENT)
                    .asPreparedStatement()
                    .withStringValue(ccy)
                    .execute();
            for (Row<String, String> row : result.getResult().getRows()) {
                ColumnList<String> cols = row.getColumns();
                
                Rate rate = new Rate();
                rate.currency = ccy;
                rate.date = cols.getDateValue("date", null); 
                rate.rate = new BigDecimal(cols.getStringValue("rate", null)); //NPE if not there - don't hide error like that, let it fail
                
                return rate;
            }
        } catch (ConnectionException e) {
            throw new RuntimeException("failed to find all rates for " + ccy, e);
        }
        
        return null;
    }
    
    public static List<Rate> findAll(String ccy) {
        return findAll(ccy, null);
    }
    
    public static List<Rate> findAll(String ccy, Integer limit) {
        AstyanaxInstance astyanax = AstyanaxFactory.getDefault();
        
        List<Rate> rates = new ArrayList<Rate>();
        try {
            OperationResult<CqlResult<String, String>> result = astyanax.getDefaultKeyspace()
                    .prepareQuery(CF)
                    .withCql(limit == null ? 
                        FIND_CCY_RATES_STATEMENT : 
                        FIND_CCY_RATES_STATEMENT + " LIMIT " + limit)
                    .asPreparedStatement()
                    .withStringValue(ccy)
                    .execute();
            for (Row<String, String> row : result.getResult().getRows()) {
                
                ColumnList<String> cols = row.getColumns();
                
                Rate rate = new Rate();
                rate.currency = ccy;
                rate.date = cols.getDateValue("date", null);
                rate.rate = new BigDecimal(cols.getStringValue("rate", null)); //NPE if null caught
                
                rates.add(rate);
            }
        } catch (ConnectionException e) {
            throw new RuntimeException("failed to find all rates for " + ccy, e);
        }
        
        return rates;
    }
    

    public static List<Date> findAvailableDates() {
        AstyanaxInstance astyanax = AstyanaxFactory.getDefault();
        
        List<Date> rates = new ArrayList<Date>();
        try {
            OperationResult<CqlResult<String, String>> result = astyanax.getDefaultKeyspace()
                    .prepareQuery(CF)
                    .withCql(FIND_ALL_DATES_STATEMENT)
                    .execute();
            for (Row<String, String> row : result.getResult().getRows()) {
                
                ColumnList<String> cols = row.getColumns();
                
                rates.add(cols.getDateValue("date", null));
            }
        } catch (ConnectionException e) {
            throw new RuntimeException("failed to find available dates", e);
        }
        
        return rates;
    }
    
    
    /**
     * Performs a batch upsert of rates. Astyanax doesn't really have a nice
     * api for this yet and so we must build a cql insert around it using the
     * BEGIN / END BATCH syntax.
     * 
     * @param rates 
     */
    public static void upsertAll(Collection<Rate> rates) {
        AstyanaxInstance astyanax = AstyanaxFactory.getDefault();
        
        StringBuilder cql = new StringBuilder("BEGIN BATCH ");

        for (Rate rate : rates) {            
            cql.append(
                    String.format("INSERT INTO %s (currency, date, rate) VALUES ('%s', '%s', '%s');", 
                    CF_NAME, rate.currency, rate.date.getTime(), rate.rate.toString())
                    );           
        }
        
        cql.append(" APPLY BATCH;");
        
        try {
            @SuppressWarnings("unused")
            OperationResult<CqlResult<String, String>> result = astyanax.getDefaultKeyspace()
                    .prepareQuery(CF).withCql(cql.toString()).execute();

        } catch (ConnectionException e) {
            //lets not pollute our API further up
            throw new RuntimeException("failed to persist batch upsert of rates", e);
        }
    }
    
    
    /**
     * Determines based on the rates passed and the latest rates in the database
     * what we should persist from the new rates passed - if any
     * 
     * @param rates 
     */
    public static void rollup(Collection<Rate> rates) {
        //TODO - determine newest in db and compare to newest in the list provided
        //and save diff
        upsertAll(rates);
    }
    
    
    @Override
    public String toString() {
        return String.format("%s %s %s", currency, date.toString(), rate.toString());
    }
    
    
    /**
     * Hash code is based on currency and date ONLY - rate is not included.
     * @return 
     */
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + (this.currency != null ? this.currency.hashCode() : 0);
        hash = 59 * hash + (this.date != null ? this.date.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Rate other = (Rate) obj;
        if ((this.currency == null) ? (other.currency != null) : !this.currency.equals(other.currency)) {
            return false;
        }
        if (this.date != other.date && (this.date == null || !this.date.equals(other.date))) {
            return false;
        }
        if (this.rate != other.rate && (this.rate == null || !this.rate.equals(other.rate))) {
            return false;
        }
        return true;
    }
    
    
    
}
