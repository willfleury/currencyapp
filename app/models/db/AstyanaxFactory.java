/*
 */
package models.db;

/**
 *
 * @author Will
 */
public class AstyanaxFactory {
    
    //eager load (avoids sync get every time)
   private static final AstyanaxInstance INSTANCE = new DefaultAstyanaxInstance(); 
   
   public static AstyanaxInstance getDefault() { 
       return INSTANCE;
   }
}
