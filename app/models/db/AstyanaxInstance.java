/*
 */
package models.db;

import com.netflix.astyanax.AstyanaxContext;
import com.netflix.astyanax.Cluster;
import com.netflix.astyanax.Keyspace;

/**
 *
 * @author Will
 */
public interface AstyanaxInstance {
    
    public AstyanaxContext<Cluster> getContext();
    
    public Keyspace getKeyspace(String name);
    
    public Keyspace getDefaultKeyspace();
    
}
