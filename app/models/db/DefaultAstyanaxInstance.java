/*
 */
package models.db;

import com.netflix.astyanax.AstyanaxContext;
import com.netflix.astyanax.Cluster;
import com.netflix.astyanax.Keyspace;
import com.netflix.astyanax.connectionpool.NodeDiscoveryType;
import com.netflix.astyanax.connectionpool.impl.ConnectionPoolConfigurationImpl;
import com.netflix.astyanax.connectionpool.impl.CountingConnectionPoolMonitor;
import com.netflix.astyanax.impl.AstyanaxConfigurationImpl;
import com.netflix.astyanax.thrift.ThriftFamilyFactory;
import java.util.Properties;
import play.Play;

/**
 *
 * @author Will
 */
public class DefaultAstyanaxInstance implements AstyanaxInstance {
    
    private final AstyanaxContext<Cluster> context;
    
    
    private static final String CLUSTER = "cassandra.cluster.name";
    private static final String KEYSPACE = "cassandra.keyspace.name";
    private static final String HOST = "cassandra.host";
    private static final String PORT = "cassandra.port";

    
    public DefaultAstyanaxInstance() {        
        Properties props = Play.configuration; //get application.conf props
        
        //build a cluster instead of keyspace as we want a single connection 
        //pool across keyspaces
        context = new AstyanaxContext.Builder()
            .forCluster(props.getProperty(CLUSTER))
            .withAstyanaxConfiguration(new AstyanaxConfigurationImpl()      
                .setDiscoveryType(NodeDiscoveryType.NONE)
            )
            .withConnectionPoolConfiguration(new ConnectionPoolConfigurationImpl("DefaultConnectionPool")
                .setPort(Integer.valueOf(props.getProperty(PORT)))
                .setMaxConnsPerHost(5)
                .setSocketTimeout(5000)
                .setSeeds(String.format("%s:%s", props.getProperty(HOST), props.getProperty(PORT)))
            )
            .withAstyanaxConfiguration(new AstyanaxConfigurationImpl()      
                .setCqlVersion("3.0.0"))
            .withConnectionPoolMonitor(new CountingConnectionPoolMonitor())
            .buildCluster(ThriftFamilyFactory.getInstance());
        

        context.start();
    }
    
   
    public AstyanaxContext<Cluster> getContext() {
        return context;
    }

    public Keyspace getKeyspace(String name) {   
        return context.getEntity().getKeyspace(name);
    }
    
    public Keyspace getDefaultKeyspace() {
        return getKeyspace(Play.configuration.getProperty(KEYSPACE));
    }
    
}
