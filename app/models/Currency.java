/*
 */
package models;

import com.netflix.astyanax.connectionpool.OperationResult;
import com.netflix.astyanax.connectionpool.exceptions.ConnectionException;
import com.netflix.astyanax.model.ColumnFamily;
import com.netflix.astyanax.model.ColumnList;
import com.netflix.astyanax.model.CqlResult;
import com.netflix.astyanax.model.Row;
import com.netflix.astyanax.serializers.StringSerializer;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import models.db.AstyanaxInstance;
import models.db.AstyanaxFactory;
import util.DateUtil;

/**
 * Model object which represents a currency.
 * 
 * This class acts as a DAO for Cassandra DB also and provides lots of helper 
 * methods for working with currencies. * 
 * 
 * @author Will
 */
public class Currency  {
    
    /**
     * precision of the operations on currencies - based on ECB
     */
    public static final int DEFAULT_DECIMAL_SCALE = 6;
    
    
    
    private static final String CF_NAME = "currencies";
    
    //version of cassandra doesn't allow key only insert - must have other column even if unused
    private static final String UPSERT_STATEMENT =  String.format("INSERT INTO %s (currency, name, last_refresh, last_rate_refresh) VALUES (?, ?, ?, ?)", CF_NAME);         

    private static final String FIND_STATEMENT =   String.format("SELECT currency, last_refresh, last_rate_refresh FROM %S where currency = ?", CF_NAME);  //check exists!
    
    private static final String FIND_ALL_STATEMENT =   String.format("SELECT currency, last_refresh, last_rate_refresh FROM %S", CF_NAME); 
    
    private static final String DELETE_STATEMENT =  String.format("DELETE FROM %S WHERE currency = ?", CF_NAME);
    
    private static final ColumnFamily<String, String> CF = ColumnFamily.newColumnFamily(
                CF_NAME, 
                StringSerializer.get(),
                StringSerializer.get());
    
    
    /**
     * Converts the amount to this currency using default precision
     * 
     * @param amt the amount to convert 
     * @param rate the rate to use in conversion
     * @return the result
     */
    public static BigDecimal convert(BigDecimal amt, BigDecimal rate) {      
         return convert(amt, rate, DEFAULT_DECIMAL_SCALE);
    }
    
    
    /**
     * Same as {@link #convert(java.math.BigDecimal, java.math.BigDecimal) } 
     * except it takes the scale as an argument
     * 
     * @param amt the amount to convert 
     * @param rate the rate to use in conversion
     * @param scale the scale to use in conversion
     * @return the result
     */
    public static BigDecimal convert(BigDecimal amt, BigDecimal rate, int scale) {        
        if (rate.equals(BigDecimal.ONE)) return amt;
        
        return rate.multiply(amt).setScale(scale, RoundingMode.HALF_UP);
    }   
    
    
    
    /**
     * Currency symbol 
     */
    public String symbol;
    
    /**
     * date the currency was last refreshed
     */
    public Date lastRefresh;
    
    /**
     * Date the currencies rates were last refreshed
     */
    public Date lastRateRefresh;

    
    
    public Currency() { }
     
    public Currency(String symbol) {
        this.symbol = symbol;
    }
    
    public Currency(String symbol, Date lastRefresh) {
        this.symbol = symbol;
        this.lastRefresh = lastRefresh;
    }
    

    /**
     * Returns the rate for the given date
     * 
     * @param date
     * @return 
     */
    public Rate findRate(Date date) {
        return Rate.find(symbol, date);
    }
    
    
    /**
     * DAO Methods
     */
    
    
    /**
     * either insert or update (upsert)
     */
    public void save() {
        AstyanaxInstance astyanax = AstyanaxFactory.getDefault();
        
        try {
            @SuppressWarnings("unused")
            OperationResult<CqlResult<String, String>> result = astyanax.getDefaultKeyspace()
                    .prepareQuery(CF)
                    .withCql(UPSERT_STATEMENT)
                    .asPreparedStatement()
                    .withStringValue(symbol)
                    .withStringValue("")
                    .withLongValue(lastRefresh.getTime())
                    .withLongValue(lastRateRefresh.getTime())
                    .execute();
        } catch (ConnectionException e) {
            throw new RuntimeException("failed to persist currency", e);
        }
    }
    
    public void delete() {
        AstyanaxInstance astyanax = AstyanaxFactory.getDefault();
        
        try {
            @SuppressWarnings("unused")
            OperationResult<CqlResult<String, String>> result = astyanax.getDefaultKeyspace()
                    .prepareQuery(CF)
                    .withCql(DELETE_STATEMENT)
                    .asPreparedStatement()
                    .withStringValue(symbol)
                    .execute();
        } catch (ConnectionException e) {
            throw new RuntimeException("failed to delete currency", e);
        }
    }
    

    
     public static Currency find(String ccy) {
        AstyanaxInstance astyanax = AstyanaxFactory.getDefault();

        try {
            OperationResult<CqlResult<String, String>> result = astyanax.getDefaultKeyspace()
                    .prepareQuery(CF)
                    .withCql(FIND_STATEMENT)
                    .asPreparedStatement()
                    .withStringValue(ccy)
                    .execute();
            for (Row<String, String> row : result.getResult().getRows()) {
                ColumnList<String> cols = row.getColumns();
                
                Currency currency = new Currency();
                
                currency.symbol = cols.getStringValue("currency", null);
                currency.lastRefresh = cols.getDateValue("last_refresh", null);
                currency.lastRateRefresh = cols.getDateValue("last_rate_refresh", null);
                
                return currency;
            }
        } catch (ConnectionException e) {
            throw new RuntimeException("failed to find all rates for " + ccy, e);
        }
        
        return null;
    }
     
    
    public static List<Currency> findAll() {
        AstyanaxInstance astyanax = AstyanaxFactory.getDefault();
        
        List<Currency> currencies = new ArrayList<Currency>();
        try {
            OperationResult<CqlResult<String, String>> result = astyanax.getDefaultKeyspace()
                    .prepareQuery(CF)
                    .withCql(FIND_ALL_STATEMENT)
                    .execute();
            for (Row<String, String> row : result.getResult().getRows()) {                
                ColumnList<String> cols = row.getColumns();
                
                Currency currency = new Currency();
                
                currency.symbol = cols.getStringValue("currency", null);
                currency.lastRefresh = cols.getDateValue("last_refresh", null);
                currency.lastRateRefresh = cols.getDateValue("last_rate_refresh", null);
                
                currencies.add(currency);
            }
        } catch (ConnectionException e) {
            throw new RuntimeException("failed to find all currencies", e);
        }
        
        return currencies;
    }


    /**
     * Performs a batch upsert of rates. Astyanax doesn't really have a nice
     * api for this yet and so we must build a cql insert around it using the
     * BEGIN / END BATCH syntax.
     * 
     * @param currencies 
     */
    public static void upsertBatch(Collection<Currency> currencies) {
        AstyanaxInstance astyanax = AstyanaxFactory.getDefault();
        
        StringBuilder cql = new StringBuilder("BEGIN BATCH ");

        for (Currency currency : currencies) {            
            cql.append(
                    String.format("INSERT INTO %s (currency, last_refresh) VALUES ('%s', '%s');", 
                    CF_NAME, currency.symbol, currency.lastRefresh.getTime())
                    );           
        }
        
        cql.append(" APPLY BATCH;");
        
        try {
            @SuppressWarnings("unused")
            OperationResult<CqlResult<String, String>> result = astyanax.getDefaultKeyspace()
                    .prepareQuery(CF).withCql(cql.toString()).execute();

        } catch (ConnectionException e) {
            //lets not pollute our API further up
            throw new RuntimeException("failed to persist batch upsert of rates", e);
        }
    }
    
    public static Collection<Currency> fromSymbols(Collection<String> ccys) {
        return fromSymbols(ccys, null);
    }
    
    public static Collection<Currency> fromSymbols(Collection<String> symbols, Date refreshDate) {
        if (symbols == null) return null;
        
        List<Currency> currencies = new ArrayList<Currency>();
        
        Date now = DateUtil.getDateComponent(new Date());
        for (String ccy : symbols)
            currencies.add(new Currency(ccy, now));
        
        return currencies;
    }
    
    public static Collection<String> toSymbols(Collection<Currency> currencies) {
        if (currencies == null) return null;
        
        List<String> symbols = new ArrayList<String>();
        for (Currency currency : currencies) 
            symbols.add(currency.symbol);
        
        return symbols;
    }

    
    @Override
    public int hashCode() {
        return symbol.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Currency other = (Currency) obj;
        if ((this.symbol == null) ? (other.symbol != null) : !this.symbol.equals(other.symbol)) {
            return false;
        }
        return true;
    }
    
    
}
