/* 
 */


/**
 * This looks after a conversion by performing an ajax call with the contents 
 * of the form 
 * 
 * convertAction - ref to action for performing conversion
 */
function handleConvert(convertAction) {

//    var resultField = document.getElementById("resultId");
    
    //now get the source amount
    var amt = document.getElementById("sourceAmount").value;
    
    //make sure amount is populated and ok - could use jquery validation but this is basic
    var floatRegex = /^((\d+(\.\d *)?)|((\d*\.)?\d+))$/;
    if (amt == "" || ! floatRegex.test(amt)) {
        alert("Source amount should be correct decimal format");
        return;
    } 

    var srcCcy = $("#convertorForm select[name=sourceCurrencySelect]").val();
    var trgtCcy = $("#convertorForm select[name=targetCurrencySelect]").val();
    
    var date = $('#convertorForm input[name=datepicker]').datepicker({ dateFormat: 'yyyy-MM-dd' }).val();
    if (!date) {
        alert("Select a date first");
        return;
    }
    
    // no error handling with the pure get for jquery that I could find so it should
    // use another approach which allowed it but I suck a js..
    $.get(
        convertAction({source: srcCcy, target: trgtCcy, amt: amt, date: date}),
        function(result) {
            document.getElementById("targetAmount").value = result;
        }
    )

}


/**
 * This fucntion performs an ajax call to the server to obtain the fx rates for the 
 * last 90 days for the selected currency.
 * It then draws a simple area chart with the data. 
 * 
 * historicAction - ref to action for the history
 * ccy - currency to fetch data for 
 */
function drawHistoricData(historicAction, ccy) {
    

    $.get(
        historicAction({ccy: ccy}), 
        function(data) {
            var plotData = [];

            // format is yyyy-MM-dd
            // should really figure out how to change the default for the json lib used by play (GSon ?)
            var parseDate = d3.time.format("%Y-%m-%d").parse;
            //map to the format d3 needs
            $.each(data, function(key, value) {
                
                 plotData.push(
                    {"date" : parseDate(value.date),
                     "close" : value.rate});
             });




             //d3 stuff - taken from website example with one or two minor adjustments
             
             var margin = {top: 20, right: 20, bottom: 30, left: 50},
                width = 800 - margin.left - margin.right,
                height = 500 - margin.top - margin.bottom;

            var x = d3.time.scale().range([0, width]);

            var y = d3.scale.linear().range([height, 0]);

            var xAxis = d3.svg.axis().scale(x).orient("bottom");

            var yAxis = d3.svg.axis().scale(y).orient("left");

            var area = d3.svg.area()
                .x(function(d) { return x(d.date); })
                .y0(height)
                .y1(function(d) { return y(d.close); });


            //removes previous one (if any) 
            d3.select("svg").remove();

            //adds the svg 
            var svg = d3.select("#charts").append("svg")
                .attr("width", width + margin.left + margin.right)
                .attr("height", height + margin.top + margin.bottom)
              .append("g")
                .attr("transform", "translate(" + margin.left + "," + margin.top + ")");


            //this just gets /sets the range for the chats axis
            x.domain(d3.extent(plotData, function(d) { return d.date; }));
            y.domain(d3.extent(plotData, function(d) { return d.close; }));

            //draws the actual chart on the svg
            svg.append("path")
                .datum(plotData)
                .attr("class", "area")
                .attr("d", area);

            svg.append("g")
                .attr("class", "x axis")
                .attr("transform", "translate(0," + height + ")")
                .call(xAxis);

            svg.append("g")
                .attr("class", "y axis")
                .call(yAxis)
              .append("text")
                .attr("transform", "rotate(-90)")
                .attr("y", 6)
                .attr("dy", ".71em")
                .style("text-anchor", "end")
                .text("FX Rate");
          }
     )
}


/**
 * calls a refresh of the rates on the server side.
 * 
 * refreshAction - ref to action performing refresh
 */
function refreshRates(refreshAction) {
    $.get(
            refreshAction(),
            function() {            
                //do something if we want to ..
            }
        )
}